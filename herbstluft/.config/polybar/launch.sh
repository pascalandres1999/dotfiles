#! /bin/sh

POLYBAR_CONFIG="$HOME/.config/polybar/config.ini"

default_interface() {
	ip route | grep '^default' | cut -d" " -f5
}

interface="$(default_interface)"


if type "xrandr" ; then
	for monitor in $(xrandr --query | grep " connected" | cut -d" " -f1); do
		INTERFACE=$interface MONITOR=$monitor polybar -c "$POLYBAR_CONFIG" --reload main
	done
else
	INTERFACE=$interface polybar -c "$POLYBAR_CONFIG" --reload main
fi
