# Lines configured by zsh-newuser-install
HISTFILE=~/.cache/zsh/history
HISTSIZE=1000
SAVEHIST=100000
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/pascal/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

#Tab completion
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)

# Vi Mode
bindkey -v
export KEYTIMEOUT=5

# Vi keys in tab completion menu
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char

# Vi Backspace
bindkey -v '^?' backward-delete-char


# {{{ node version manager

alias_node_commands() {
	alias nvm="unalias nvm node npm yarn && . "$NVM_DIR"/nvm.sh && nvm"
	alias node="unalias nvm node npm yarn && . "$NVM_DIR"/nvm.sh && node"
	alias npm="unalias nvm node npm yarn && . "$NVM_DIR"/nvm.sh && npm"
	alias yarn="unalias nvm node npm yarn && . "$NVM_DIR"/nvm.sh && yarn"
}
if [ -s "$HOME/.nvm/nvm.sh" ]; then
	export NVM_DIR="$HOME/.nvm"
	alias_node_commands
fi
#}}}

# Prompt
function zle-line-init zle-keymap-select {
	#PS1="%F{blue}|%f%F{magenta}%1~%f%\%F{blue}|%f%(?.>.%F{red}>%f) "
	PS1="%F{blue}|%f%F{magenta}%1~%f%\%F{blue}|%f${${KEYMAP/vicmd/%F{green\}N%f}/(main|viins)/>} "
	RPS1="%(?..%F{red}%?%f)"
	echo -ne '\e[3 q'
	zle reset-prompt
}

zle -N zle-line-init
zle -N zle-keymap-select

# Syntax highlighting

ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern cursor)
source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2> /dev/null

source "$HOME/.config/zsh/aliasrc"
source "$HOME/.config/zsh/envrc"


