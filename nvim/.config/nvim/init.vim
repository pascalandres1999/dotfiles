"{{{ VimPlug Bootstrap
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif
"}}}

"{{{ Plugins
call plug#begin()
Plug 'itchyny/lightline.vim'
Plug 'justinmk/vim-syntax-extra'
Plug 'wellle/targets.vim'
Plug '~/.fzf'
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
Plug 'wincent/terminus'
Plug 'pangloss/vim-javascript'
Plug 'moll/vim-node'
Plug 'MaxMEllon/vim-jsx-pretty'
Plug 'dylanaraps/wal.vim'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'mattn/emmet-vim'
Plug 'vimwiki/vimwiki'
Plug 'justinmk/vim-sneak'
Plug 'dhruvasagar/vim-table-mode'
Plug 'joshdick/onedark.vim'
Plug 'tpope/vim-commentary'
Plug 'HerringtonDarkholme/yats.vim'
Plug 'arzg/vim-rust-syntax-ext'
Plug 'DingDean/wgsl.vim'
call plug#end()
"}}}

"{{{ Vim Color Theme
colorscheme onedark
syntax on
hi Normal guibg=NONE ctermbg=NONE
"}}}

"Relative Line Numbers
set number relativenumber

set completeopt-=preview
set backspace=2

"LightLine
set laststatus=1

"Stop Concealing
set conceallevel=0

"Tab size
filetype plugin indent on
set tabstop=2
set shiftwidth=2

"Set Leader to ,
let mapleader=","

"Fzf mappings
nnoremap <leader>v :Files<Enter>
nnoremap <leader>c :Commands<Enter>
nnoremap <leader>b :Buffers<Enter>

autocmd BufRead,BufNewFile ~/.dotfiles/*,~/.config/*,~/.tmux.conf,~/.zshrc set foldmethod=marker

" Disables automatic commenting on newline:
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o"

"Leader Jumping"
inoremap <leader><leader> <Esc>/<++><Enter>"_c4l
vnoremap <leader><leader> <Esc>/<++><Enter>"_c4l
map <leader><leader> <Esc>/<++><Enter>"_c4l

"Tab Controls
map <leader>th :tabfirst<Enter>
map <leader>tl :tablast<Enter>
map <leader>tj :tabprevious<Enter>
map <leader>tk :tabnext<Enter>
map <leader>tn :tabnew<Enter>

"Spell Checker"
map <F6> :setlocal spell! spelllang=de_de<CR>

"Emu File Type
au BufRead,BufNewFile *.emu set filetype=emu

"{{{ VimWiki
let g:vimwiki_list = [{'path': '~/documents/wiki/',
                      \ 'syntax': 'markdown', 'ext': '.md',
											\ 'nested_syntaxes' : {'c': 'c', 'py' : 'python', 'sh' : 'sh'},
											\ 'links_space_char' : '_',
											\ 'auto_diary_index' : 1},
											\ {'path': '~/documents/ctfs/wiki',
											\ 'syntax' : 'markdown', 'ext': '.md',
											\ 'nested_syntaxes' : {'py' : 'python'},
											\ 'links_space_char' : '_'}]

"Disable wrapping in wiki dirs
autocmd BufRead,BufNewFile ~/documents/wiki/*,~/documents/ctfs/wiki/* set nowrap


"Table mode
let g:vimwiki_table_auto_fmt = 0
let g:table_mode_syntax = 1
map <leader>tm :TableModeToggle


"Custom vimwiki file handler
function! VimwikiLinkHandler(link)
	let link = a:link
	if link =~ "local:"
		let link = link[6:]
		let open_program = '/home/pascal/.local/bin/open_detach'
		try
			let filename = vimwiki#base#resolve_link(link).filename
			if filename == ''
				echo 'unable to resolve link'
				return 0
			endif
			execute '!open_detach "' . filename . '"'
		catch
			echo "could not open"
		endtry
		return 1
	else
		return 0
	endif
endfunction
"}}}

" Latex
let g:tex_flavor = "latex"

set clipboard=unnamedplus

"{{{ Coc Settings

"{{{2 Extentions
let g:coc_global_extentions = [
	\ 'coc-tsserver',
	\ 'coc-json',
	\ 'coc-lists',
	\ 'coc-pairs',
	\ 'coc-rust-analyzer',
	\ 'coc-snippets',
	\ ]
"}}}2

" if hidden is not set, TextEdit might fail.
set hidden

" Some servers have issues with backup files, see #649
set nobackup
set nowritebackup

" Better display for messages
set cmdheight=2

" You will have bad experience for diagnostic messages when it's default 4000.
set updatetime=300

" don't give |ins-completion-menu| messages.
set shortmess+=c

" always show signcolumns
set signcolumn=yes

" Use tab for trigger completion with characters ahead and navigate.
" Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
	let col = col('.') - 1
		return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current position.
" Coc only does snippet and additional edit on confirm.
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
" Or use `complete_info` if your vim support it, like:
" inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"

" Use `[g` and `]g` to navigate diagnostics
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
	if (index(['vim','help'], &filetype) >= 0)
		execute 'h '.expand('<cword>')
	else
		call CocAction('doHover')
	endif
endfunction

" Highlight symbol under cursor on CursorHold
autocmd CursorHold * silent call CocActionAsync('highlight')

" Remap for rename current word
nmap <leader>r <Plug>(coc-rename)

" Remap for format selected region
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
autocmd!
" Setup formatexpr specified filetype(s).
autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
" Update signature help on jump placeholder
autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Remap for do codeAction of selected region, ex: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap for do codeAction of current line
nmap <leader>ac  <Plug>(coc-codeaction)
" Fix autofix problem of current line
nmap <leader>qf  <Plug>(coc-fix-current)

" Create mappings for function text object, requires document symbols feature of languageserver.
xmap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap if <Plug>(coc-funcobj-i)
omap af <Plug>(coc-funcobj-a)

" Use <TAB> for select selections ranges, needs server support, like: coc-tsserver, coc-python
nmap <silent> <TAB> <Plug>(coc-range-select)
xmap <silent> <TAB> <Plug>(coc-range-select)

" Use `:Format` to format current buffer
command! -nargs=0 Format :call CocAction('format')

" Use `:Fold` to fold current buffer
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" use `:OR` for organize import of current buffer
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Add status line support, for integration with other plugin, checkout `:h coc-status`
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

"{{{2 CocList
" Show all diagnostics
nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions
nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>
" Show commands
nnoremap <silent> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document
nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols
nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list
nnoremap <silent> <space>p  :<C-u>CocListResume<CR>
"}}}2
"}}}

map <leader>e :CocCommand eslint.executeAutofix<Enter>

autocmd Filetype tex inoremap <leader>lq "`"'<Esc>hha
